"use strict";

const Promise = require("bluebird");
const AModule = require("anna/AModule");
const redis = require('redis');

Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);

module.exports = class RedisModule extends AModule {
    constructor (core, settings) {
        super(core, settings);
    }

    get client () {
        return this._redisClient;
    }

    init () {
        this.log.debug(`${this.constructor.name} module init()`);
        return new Promise((resolve, reject) => {
            setImmediate(_ => {
                this._redisClient = redis.createClient(this.settings.connection);

                this.client.once("error", err => {
                    reject(`Cannot connect to Redis: ${err.stack}`);
                });

                this.client.on("ready", resolve);
            });
        });
    }
}
